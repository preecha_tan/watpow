import axios from "axios"
// import { useDispatch } from "react-redux";
// import { setToken } from "./action/LoginAction";

// export const host = "http://192.168.1.5:8080";
export const host = "https://nabiigale.strangled.net"
export function signUp(email, password, firstname, lastname) {
    return axios.post(`${host}/signup`, { email, password, firstname, lastname });
}

export function listTemple() {
    return axios.get(`${host}/temples`);
}

export function listOneTemple(temple_id) {
    return axios.get(`${host}/temples/${temple_id}`);
}

export function listMilestone(temple_id) {
    return axios.get(`${host}/temples/${temple_id}/milestones`);
}

export function createMilestone(temple_id, data, token) {
    return axios.post(`${host}/protected/temples/${temple_id}/milestones`, data, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function createEvent(temple_id, data, token) {
    return axios.post(`${host}/protected/events/${temple_id}`, data, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function listEvent() {
    return axios.get(`${host}/events`);
}

// export function createTemple({ name, opcl, location, district, tel, website, type, latitude, longitude }, token) {

//     return axios.post(`${host}/protected/temples`, { name, opcl, location, district, tel, website, type, latitude, longitude }, {
//         headers: {
//             Authorization: `Bearer ${token}`
//         }
//     });
// }