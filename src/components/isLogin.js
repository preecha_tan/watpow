import React from 'react'
import { View, Text, Image } from 'react-native'
import { IconButton, Button } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import { Row, } from 'native-base';
import { useDispatch } from 'react-redux';
import { setToken } from '../reducers/LoginAction';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default function IsLogin() {

    const navigation = useNavigation();

    const dispatch = useDispatch();

    const onLogout = () => {
        dispatch(setToken(null))
    }

    return (
        <View>
            <View style={{ alignItems: 'center' }}>
                <View style={{ marginTop: '20%', }}>
                    <Image source={require('../assests/avt.png')} style={{ width: 200, height: 200, borderRadius: 100 }}></Image>
                </View>

                {/* <View style={{}}>
                    <Text style={{ fontSize: 20, marginBottom: 10, marginTop: 10 }}>ชื่อ : </Text>
                    <Text style={{ fontSize: 20 }}>นามสกุล : </Text>
                </View> */}

            </View >
            <View style={{ marginTop: 20 }}>
                <Button icon="plus" mode="contained"
                    onPress={() => navigation.navigate('CreateTemple')}
                    style={{ marginLeft: 30, marginRight: 30, marginTop: 15, marginBottom: 15 }}
                    color="#FFD700"
                >
                    เพิ่มวัด
                </Button>

                {/* <Button icon="camera" mode="contained" onPress={() => navigation.navigate('CreateMilestone')} style={{ marginLeft: 30, marginRight: 30, marginBottom: 15 }} color="#FFD700">
                    รายชื่อวัด
                </Button> */}

                <Button icon="" mode="contained" onPress={() => onLogout()} style={{ marginLeft: 30, marginRight: 30 }} color="red">
                    ออกระบบ
                </Button>
            </View>
        </View>

    )
}
