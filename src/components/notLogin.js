import React from 'react'
import { View, Text } from 'react-native'
import { IconButton, Button } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { ScrollView } from 'react-native-gesture-handler';


export default function NotLogin() {

    const navigate = useNavigation();

    return (

        <ScrollView >
            <View style={{ alignItems: "center", marginTop: 200 }}>
                <Icon
                    name="times-circle"
                    color="black"
                    size={150}
                />
                <Text style={{ fontSize: 16, marginTop: 10 }}>กรุณาเข้าสู่ระบบก่อนใช้งาน</Text>
            </View>
            <View style={{ alignItems: "center", margin: 50 }}>
                <Button icon="account-arrow-right" color="#FFD700" mode="contained" onPress={() => navigate.navigate('Login')}>
                    คลิ๊กเพื่อเข้าสู่ระบบ
                </Button>
            </View>
        </ScrollView>

    )
}
