import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';

import HomeScreen from '../screens/HomeScreen';
import ProfileScreen from '../screens/ProfileScreen';
import MapScreen from '../screens/MapScreen';
import { ListEventScreen } from '../screens/ListEventScreen';

const Tab = createBottomTabNavigator();

export function HomeRouter() {

    return (
        <Tab.Navigator
            initialRouteName="Home"
            tabBarOptions={{
                // inactiveBackgroundColor: '#FFD700',
                // activeBackgroundColor: '#FFD700',
                activeTintColor: 'black',
            }}
        >
            <Tab.Screen name="Event" component={ListEventScreen}
                options={{
                    tabBarLabel: 'Event',
                    tabBarIcon: ({ color, size }) => (
                        <Icon name="calendar-alt" color={color} size={size} />
                    ),
                }}
            />

            <Tab.Screen name="Map" component={MapScreen}
                options={{
                    tabBarLabel: 'Map',
                    tabBarIcon: ({ color, size }) => (
                        <Icon name="compass" color={color} size={size} />
                    ),
                }}
            />

            <Tab.Screen name="Home" component={HomeScreen}
                options={{
                    tabBarLabel: 'Temple',
                    tabBarIcon: ({ color, size }) => (
                        <Icon name="synagogue" color={color} size={size} />
                    ),
                }}
            />
            <Tab.Screen name="Profile" component={ProfileScreen}
                options={{
                    tabBarLabel: 'Profile',
                    tabBarIcon: ({ color, size }) => (
                        <Icon name="ellipsis-h" color={color} size={size} style={{ paddingTop: 6 }} />
                    ),
                }}
            />
        </Tab.Navigator>
    )
}
export default HomeRouter;
