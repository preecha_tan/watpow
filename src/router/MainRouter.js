import React from 'react'
import { Provider } from 'react-redux'
import { createStore } from 'redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import loginReducer from '../reducers/loginReducer';
import 'react-native-gesture-handler';
import LoginScreen from '../screens/LoginScreen';
import HomeRouter from '../router/HomeRouter';
import HomeScreen from '../screens/HomeScreen';
import TempleInfoScreen from '../screens/TempleInfoScreen';
import SignupScreen from '../screens/SignupScreen';
import CreateTemple from '../screens/CreateTemple';
import { createMilestone } from '../../api';
import CreateEvent from '../screens/CreateEvent';
import { EventInfoScreen } from '../screens/EventInfoScreen';


const store = createStore(loginReducer);

const Stack = createStackNavigator();

function RouterMain() {
    return (
        <Provider store={store}>
            <NavigationContainer>
                <Stack.Navigator initialRouteName={HomeRouter} screenOptions={{ headerShown: false }} >
                    <Stack.Screen name="HomeRouter" component={HomeRouter} options={{ title: 'HomeRouter' }} />
                    <Stack.Screen name="Login" component={LoginScreen} />
                    <Stack.Screen name="Signup" component={SignupScreen} />
                    <Stack.Screen name="Home" component={HomeScreen} options={{ title: 'Home' }} />
                    <Stack.Screen name="TempleInfo" component={TempleInfoScreen} options={{ title: 'One Temple' }} />
                    <Stack.Screen name="CreateMilestone" component={createMilestone} />
                    <Stack.Screen name="CreateTemple" component={CreateTemple} />
                    <Stack.Screen name="CreateEvent" component={CreateEvent} />
                    <Stack.Screen name="EventInfoScreen" component={EventInfoScreen} />
                </Stack.Navigator>
            </NavigationContainer>
        </Provider>
    )
}
export default RouterMain;