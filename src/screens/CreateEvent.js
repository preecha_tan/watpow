import React, { useState, useRef, useEffect } from 'react'
import { View, Text, KeyboardAvoidingView, StyleSheet } from 'react-native'
import { IconButton, Button, TextInput } from 'react-native-paper';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import { createEvent } from '../../api';
import { useSelector } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';



import { useNavigation } from '@react-navigation/native';
import { Header, Left, Body, Right } from 'native-base';


export default function CreateEvent(props) {

    const token = useSelector(state => state.token)
    console.log(token);


    const navigation = useNavigation()

    const [event, setEvent] = useState("")
    const [description, setDescription] = useState("")
    const [dob, setDob] = useState(new Date())
    const parseDate = moment.utc(dob, "yyyy-MM-dd")

    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const item = props.route.params.item
    const temple_id = item.ID

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const handleConfirm = (date) => {
        console.warn("A date has been picked: ", date);
        setDob(date)
        setDatePickerVisibility(false)
    };


    const onCreateEvent = () => {

        const data = {
            "eventname": event,
            "description": description,
            "date": parseDate
        }

        createEvent(temple_id, data, token)
            .then(response => {
                console.log("response", response.data);
                navigation.navigate('TempleInfo')
            })
            .catch(err => {
                console.log("Error", err)
            });
    }

    return (
        <View >
            <View>
                <Header style={styles.header}>
                    <Left>
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                        >
                            <Icon name="chevron-left" size={28} color="black"></Icon>
                        </TouchableOpacity>
                    </Left>
                    <Body style={styles.body}>
                        <Text style={{ fontSize: 24 }}>สร้างกิจกรรม</Text>
                    </Body>
                    <Right></Right>
                </Header>
            </View>
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                <View style={{ margin: '10%', marginTop: "2%" }}>
                    <Text style={{ marginBottom: 20, textAlign: 'center', fontSize: 24 }}>กรอกข้อมูลกิจกรรม</Text>
                    <TextInput style={{ marginBottom: 20 }}
                        // mode='outlined'
                        label='กิจกรรม'
                        value={event}
                        onChangeText={value => setEvent(value)}
                    />
                    <TextInput style={{ marginBottom: 20 }}
                        // mode='outlined'
                        label='รายละเอียด'
                        value={description}
                        onChangeText={value => setDescription(value)}
                    />
                    <Button style={{ marginBottom: 15 }} mode='contained' color='gray' onPress={showDatePicker} >
                        <Text>เลือกวันที่จัดกิจกรรม</Text>
                    </Button>
                    <DateTimePickerModal
                        isVisible={isDatePickerVisible}
                        mode="date"
                        onConfirm={handleConfirm}
                    // onCancel={hideDatePicker}
                    />

                    <Button icon="" mode="contained" onPress={() => onCreateEvent()} style={{ marginBottom: 20 }} color="#FFD700">
                        สร้าง
                    </Button>
                </View >
            </ScrollView>
        </View>
    )
}
const styles = StyleSheet.create({
    headertext: {
        fontSize: 24,
    },
    body: {
        alignItems: "center",
        justifyContent: "center",
    },
    header: {
        borderBottomWidth: 0,
        borderBottomColor: 'black',
        backgroundColor: '#FFD700',
    }
});