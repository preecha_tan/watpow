import React, { useState, useRef, useEffect } from 'react'
import { View, Text, KeyboardAvoidingView, StyleSheet, Image } from 'react-native'
import { IconButton, Button, TextInput } from 'react-native-paper';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import { host } from '../../api';
import { useSelector } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome5';

import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'
import { check, request, PERMISSIONS, RESULTS } from 'react-native-permissions'
import Geolocation from 'react-native-geolocation-service';

import { useNavigation } from '@react-navigation/native';
import { Header, Left, Body, Right } from 'native-base';
import axios from 'axios';

import ImagePicker from 'react-native-image-picker';

export default function CreateTemple(props) {

    const MapViewRef = useRef()

    const navigation = useNavigation()

    const [coordinate, setCoordinate] = useState({
        latitude: 37.78825,
        longitude: -122.4324
    })
    const [name, setName] = useState("")
    const [opcl, setOpcl] = useState("")
    const [location, setLocation] = useState("")
    const [tel, setTel] = useState("")
    const [website, setWebsite] = useState("")
    const [type, setType] = useState("")
    const [district, setDistrict] = useState("")
    const [photo, setPhoto] = useState('');

    const [dataPhoto, setDataPhoto] = useState({});

    const token = useSelector(state => state.token)



    useEffect(() => {

        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
            .then(result => {
                if (result === RESULTS.GRANTED) {
                    callLocation((position) => {
                        setCoordinate(position.coords)
                        MapViewRef.current.animateCamera({
                            center: {
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude,
                            }
                        })
                    })
                }
                if (result === RESULTS.DENIED) {
                    request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
                        .then(result => {
                            if (result === RESULTS.GRANTED) {
                                callLocation((position) => {
                                    setCoordinate(position.coords)
                                    MapViewRef.current.animateCamera({
                                        center: {
                                            latitude: position.coords.latitude,
                                            longitude: position.coords.longitude,
                                        }
                                    })
                                })
                            }
                            else if (result === RESULTS.DENIED) {
                                alert('Please accept')
                            }
                            else if (result === RESULTS.BLOCKED) {
                                alert('Thank you')
                            }
                        })
                    return
                }
                if (result === RESULTS.BLOCKED) {
                    alert('...')
                }
            })
    }, [])
    console.log('datanow', dataPhoto);

    const addPhoto = () => {
        const options = {
            title: 'Select Photo',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                const uri = response.uri;
                const type = response.type;
                const name = response.fileName;
                const source = {
                    uri,
                    type,
                    name,
                };
                setPhoto(response);
                setDataPhoto(source);
            }
        });
    };
    const cloudinaryUpload = () => {
        const data = new FormData();
        console.log('photo', dataPhoto);

        data.append('file', dataPhoto);
        data.append('upload_preset', 'k5f7rmvz');
        data.append('cloud_name', 'delkt035z');
        fetch('https://api.cloudinary.com/v1_1/delkt035z/image/upload', {
            method: 'post',
            body: data,
        })
            .then(res => res.json())
            .then(data => {
                console.log('url===============>', data.secure_url);
                // UpdateImage({image: data.secure_url}, token)
                //   .then(res => console.log('urlllll===============>', res.data))
                //   .catch(err => console.log('err==>', err));
                onCreateTemple(data.secure_url);
            })
            .catch(err => {
                alert('An Error Occured While Uploading');
                console.log('error------------------>', err);
            });
    };


    const callLocation = (callback) => {
        Geolocation.getCurrentPosition(
            (position) => {
                callback(position)
            },
            (error) => {
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        )
    }

    const onCreateTemple = (photo) => {
        console.log("Photo: ", photo)
        axios.post(`${host}/protected/temples`, { name, opcl, location, district, tel, website, type, latitude: coordinate.latitude.toString(), longitude: coordinate.longitude.toString(), image: photo },
            {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(response => {
                console.log("response", response.data);
                navigation.navigate('Profile')
            })
            .catch(err => {
                console.log("Error", err)
            });
    }

    return (
        <View style={{ backgroundColor: '#FFA07A' }}>
            <View>
                <Header style={styles.header}>
                    <Left>
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                        >
                            <Icon name="chevron-left" size={28} color="black"></Icon>
                        </TouchableOpacity>
                    </Left>
                    <Body style={styles.body}>
                        <Text style={{ fontSize: 24 }}>เพิ่มข้อมูลวัด</Text>
                    </Body>
                    <Right></Right>
                </Header>
            </View>
            {/* //////////////////////////////////////IMG??????????????????????????????????? */}

            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                <View
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                    <TouchableOpacity>
                        {photo === '' || photo === undefined ? (
                            <TouchableOpacity onPress={addPhoto}>
                                <Image
                                    resizeMode="cover"
                                    source={require('../assests/avt.png')}
                                    style={{ width: 200, height: 200, borderRadius: 100 }}
                                />
                            </TouchableOpacity>
                        ) : (
                                <TouchableOpacity onPress={addPhoto}>
                                    <Image source={photo}
                                        style={{ width: 200, height: 200, borderRadius: 100 }}
                                    />
                                </TouchableOpacity>
                            )}
                    </TouchableOpacity>
                </View>

                <View style={{ margin: '10%', marginTop: "2%" }}>
                    <Text style={{ marginBottom: 20, textAlign: 'center', fontSize: 24 }}>กรอกข้อมูลวัด</Text>
                    <TextInput style={{ marginBottom: 20 }}
                        // mode='outlined'
                        label='ชื่อวัด'
                        value={name}
                        onChangeText={value => setName(value)}
                    />
                    <TextInput style={{ marginBottom: 20 }}
                        // mode='outlined'
                        label='เวลาเข้าชม'
                        value={opcl}
                        onChangeText={value => setOpcl(value)}
                    />
                    <TextInput style={{ marginBottom: 20 }}
                        // mode='outlined'
                        label='สถานที่'
                        value={location}
                        onChangeText={value => setLocation(value)}
                    />
                    <TextInput style={{ marginBottom: 20 }}
                        // mode='outlined'
                        label='เบอร์โทรศัพท์'
                        value={tel}
                        onChangeText={value => setTel(value)}
                    />
                    <TextInput style={{ marginBottom: 20 }}
                        // mode='outlined'
                        label='เว็ปไซต์'
                        value={website}
                        onChangeText={value => setWebsite(value)}
                    />
                    <TextInput style={{ marginBottom: 20 }}
                        // mode='outlined'
                        label='เสริมโชค'
                        value={type}
                        onChangeText={value => setType(value)}
                    />
                    <TextInput style={{ marginBottom: 20 }}
                        // mode='outlined'
                        label='จังหวัด'
                        value={district}
                        onChangeText={value => setDistrict(value)}
                    />
                    <View>
                        <View style={{
                            height: 300,

                            margin: 20
                        }}>
                            <MapView
                                ref={MapViewRef}
                                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                                style={styles.map}
                                region={{
                                    latitude: coordinate.latitude,
                                    longitude: coordinate.longitude,
                                    latitudeDelta: 0.015,
                                    longitudeDelta: 0.0121,
                                }}
                            >
                                <Marker draggable
                                    coordinate={coordinate}
                                    onDragEnd={(event) => setCoordinate(event.nativeEvent.coordinate)}
                                />
                            </MapView>
                        </View>
                    </View>

                    <Button icon="plus" mode="contained" onPress={cloudinaryUpload} style={{ marginBottom: 20 }} color="#FFD700">
                        สร้าง
                    </Button>
                    {/* <Button
                        // style={style.buttonSubmit}
                        mode="Contained"
                        onPress={cloudinaryUpload}>
                        <Text>Submit</Text>
                    </Button> */}

                </View >
            </ScrollView>
        </View>
    )
}
const styles = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    },

    headertext: {
        fontSize: 24,
    },
    body: {
        alignItems: "center",
        justifyContent: "center",
    },
    header: {
        borderBottomWidth: 0,
        borderBottomColor: 'black',
        backgroundColor: '#FFD700',
    }
});