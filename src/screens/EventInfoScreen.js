import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, SafeAreaView, Image, ScrollView } from 'react-native'
import { Header, Left, Body, Right, } from 'native-base';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useNavigation } from '@react-navigation/native';
import { Badge, Button, Modal, Portal } from 'react-native-paper';
import { TextInput } from 'react-native-paper';
import { useSelector } from 'react-redux';
import { createMilestone, listMilestone, listOneTemple } from '../../api';
import { setToken } from '../reducers/LoginAction';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';


export function EventInfoScreen(props) {

    const templeid = props.route.params.templeid

    const navigation = useNavigation()

    const [showModal, setShowModal] = useState(false)
    const [eventname, setEventname] = useState("")
    const [description, setDescription] = useState("")
    const [dob, setDob] = useState(new Date())
    const [item, setItem] = useState({})
    const [milestone, setMilestone] = useState("")

    const parseDate = moment.utc(dob, "yyyy-MM-dd")

    const token = useSelector(state => state.token)

    const navigate = useNavigation();




    //date time picker
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const handleConfirm = (date) => {
        console.warn("A date has been picked: ", date);
        setDob(date)
        setDatePickerVisibility(false)
    };
    //date time picker

    useEffect(() => {
        listOneTemple(templeid)
            .then((data) => {
                setItem(data.data);
                console.log("dsadasdasdasdsd", data.data);
            })
            .catch((err) => {
                console.log("ERROR", err);
            });

        listMilestone(templeid)
            .then((data) => {
                setMilestone(data.data);
                console.log("dsadasdasdasdsd", data.data);
            })
            .catch((err) => {
                console.log("ERROR", err);
            });
    }, []);

    const _renderMilestone = ({ item }) => {

        return (
            <View style={{ marginBottom: 15 }}>
                <Text style={{ fontSize: 16 }}>วัน : {item.eventname}</Text>
                <Text style={{ fontSize: 16 }}>รายละเอียด : {item.description}</Text>
                <Text style={{ fontSize: 16 }}>วันที่ : {moment(item.date).format("MMM Do YY")}</Text>
            </View>
        )
    }

    const onSubmitMilestone = () => {
        console.log("createMilestone");
        const data = {
            "eventname": eventname,
            "description": description,
            "date": parseDate
        }
        console.log("daat", data);

        createMilestone(temple_id, data, token)
            .then(response => {

                console.log("ressssssssssssssssssssssponse", response.data);

                // dispatch(setToken(response.data.token))
                // localStorage.setItem("token", response.data.token)
                if (token != "" || token != null) {
                    setEventname("")
                    setDescription("")
                    setShowModal(false)
                }
            })
            .catch(err => {
                console.log("Err have token : ", token);
                console.log("Error", err)
            });
        console.log("id", temple_id);
        console.log("desc", description);
    }

    return (
        <SafeAreaView style={{ flex: 1, height: '100%' }}>
            <View>
                <Header style={styles.header}>
                    <Left>
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                        >
                            <Icon name="chevron-left" size={28} color="black"></Icon>
                        </TouchableOpacity>
                    </Left>
                    <Body style={styles.body}>
                        <Text styles={styles.text}></Text>
                    </Body>
                    <Right>
                        <Button style={{ marginRight: 15 }} icon="plus" mode="contained" color='#FFD700'
                            onPress={() => navigation.navigate('CreateEvent', { item })}
                        >
                            <Text>สร้างกิจกรรม</Text>
                        </Button>
                    </Right>
                </Header>
            </View>

            <View style={{ height: '100%' }}>
                <Image
                    source={{ uri: item.image }}
                    style={{ height: '25%' }}
                ></Image>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                    <View style={{ marginLeft: 20, marginBottom: 10 }}>
                        <View style={{ flexDirection: 'row', marginBottom: 15, marginTop: 20 }}>
                            <Text style={{ fontSize: 24, }}>{item.name}</Text>
                            <Badge danger style={{ marginLeft: 10, marginBottom: 5 }} >
                                <Text style={{ fontSize: 14 }}>{item.type}</Text>
                            </Badge>
                        </View>
                        {item.opcl === "" || item.opcl === null ? <Text style={styles.infocontent}>เวลาเข้าชม : - </Text> : <Text style={styles.infocontent}>เวลาเข้าชม :  {item.opcl}</Text>}
                        {item.location === "" || item.opcl === null ? <Text style={styles.infocontent}>สถานที่ : - </Text> : <Text style={styles.infocontent}>สถานที่ : {item.location}</Text>}
                        {item.district === "" || item.opcl === null ? <Text style={styles.infocontent}>จังหวัด : - </Text> : <Text style={styles.infocontent}>จังหวัด : {item.district}</Text>}
                        {item.tel === "" || item.opcl === null ? <Text style={styles.infocontent}>เบอร์โทรศัพท์ : - </Text> : <Text style={styles.infocontent}>เบอร์โทรศัพท์ : {item.tel}</Text>}
                        {item.website === "" || item.opcl === null ? <Text style={styles.infocontent}>เว็ปไซต์ : - </Text> : <Text style={styles.infocontent}>เว็ปไซต์ : {item.website}</Text>}
                    </View>

                    {/* <Button style={{}} icon="camera" mode="contained" onPress={() => setShowModal(true)}>
                        <Text style={{ fontSize: 18 }}>เพิ่มวันสำคัญ</Text>
                    </Button> */}
                    {/* <View style={{ marginLeft: 20 }}>
                        <Text style={{ fontSize: 24, marginTop: 10 }}>กิจกรรมวันสำคัญ</Text>
                        <FlatList
                            data={temples}
                            renderItem={_renderMilestone}
                            keyExtractor={item => item.id}
                        />
                    </View> */}
                </ScrollView>

            </View >
            <Modal visible={showModal} onDismiss={() => setShowModal(false)}>

                <View style={{ marginLeft: 15, marginRight: 15, justifyContent: 'center', height: 350, backgroundColor: 'white', borderRadius: 10 }}>
                    <Text style={{ fontSize: 22, textAlign: 'center', margin: 10 }}>เพิ่มวันสำคัญ</Text>
                    <TextInput style={{ margin: 10 }}
                        label='เทศกาล'
                        value={eventname}
                        onChangeText={value => setEventname(value)}
                    />
                    <TextInput style={{ margin: 10 }}
                        label='รายละเอียด'
                        value={description}
                        onChangeText={value => setDescription(value)}
                    />

                    <View style={{ margin: 15 }}>
                        <Button title="Show Date Picker" onPress={showDatePicker} >
                            <Text>เลือกวันที่จัดกิจกรรม</Text>

                        </Button>
                        {/* <Text>{dob}</Text> */}
                    </View>
                    <Button style={{ margin: 15 }} mode="contained" onPress={onSubmitMilestone}>
                        <Text>เพิ่ม</Text>
                    </Button>
                </View>

                <DateTimePickerModal
                    isVisible={isDatePickerVisible}
                    mode="date"
                    onConfirm={handleConfirm}
                // onCancel={hideDatePicker}
                />
            </Modal>

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        minHeight: 360,
    },
    headertext: {
        fontSize: 24,
    },
    body: {
        alignItems: "center",
        justifyContent: "center",
    },
    header: {
        borderBottomWidth: 0,
        borderBottomColor: 'black',
        // backgroundColor: '#FFD700',
        backgroundColor: 'transparent',
    },
    infocontent: {
        fontSize: 16,
        marginBottom: 10
    }
});