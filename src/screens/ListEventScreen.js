import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, SafeAreaView, RefreshControl } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'
import { listEvent } from '../../api';
import { Header, Left, Body } from 'native-base';
import moment from 'moment';

export function ListEventScreen() {

    const navigation = useNavigation()
    const [events, setEvents] = useState([])
    const [refresh, setRefresh] = useState(false)

    const fetch = () => {
        listEvent()
            .then((data) => {
                setEvents(data.data);
                console.log("dsadasdasdasdsd", data.data);
            })
            .catch((err) => {
                console.log("ERROR", err);
            });
    }

    useEffect(() => {
        fetch()
    }, []);

    const _renderEvent = ({ item, index }) => {

        return (

            <View style={{ marginLeft: 25, marginRight: 25, marginTop: 20, backgroundColor: 'white', borderRadius: 10 }}>
                <TouchableOpacity onPress={() => navigation.navigate('EventInfoScreen', { templeid: item.templeid })} >
                    <View style={{ paddingLeft: 20 }}>
                        <Text style={{ fontSize: 18 }}>กิจกรรม : {item.eventname}</Text>
                        <Text style={{ fontSize: 18 }}>รายละเอียด : {item.description}</Text>
                        <Text style={{ fontSize: 18 }}>เวลา : {moment(item.date).format("MMM Do YY")}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <SafeAreaView>
            <View>
                <Header style={styles.header}>
                    <Body style={styles.body}>
                        <Text style={{ fontSize: 28 }}>กิจกรรมทั้งหมด</Text>
                    </Body>
                </Header>
            </View>

            <ScrollView refreshControl={<RefreshControl refreshing={refresh} onRefresh={fetch} />}>
                <View style={{}}>
                    <View style={{ flex: 1 }}>
                        <FlatList
                            style={{}}
                            data={events}
                            renderItem={_renderEvent}
                            keyExtractor={item => item.id}
                        />
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>

    )
}
const styles = StyleSheet.create({
    scrollview: {
        flex: 1,
        backgroundColor: "#F7F7F7"
    },
    headertext: {
        fontSize: 24,
    },
    body: {
        alignItems: "center",
        justifyContent: "center",
    },
    header: {
        borderBottomWidth: 0,
        borderBottomColor: 'black',
        backgroundColor: '#FFD700',
    }
})