import React, { useState } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { TextInput } from 'react-native-paper';
import { useSelector, useDispatch } from 'react-redux';
import { Button } from 'react-native-paper';
import { host } from '../../api';
import axios from 'axios';
import { setToken } from '../reducers/LoginAction';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';

export function LoginScreen(props) {

    const [email, setUserText] = useState("")
    const [password, setPassText] = useState("")

    const token = useSelector(state => state.token)

    const dispatch = useDispatch()

    const onSignin = () => {

        axios.post(`${host}/login`, { email, password })
            .then(response => {

                console.log("ressssssssssssssssssssssponsetokenn", response.data);

                dispatch(setToken(response.data.token))
                if (token != "" || token != null) {
                    setPassText("")
                    setUserText("")

                } props.navigation.navigate('HomeRouter')
            })
            .catch(err => {
                console.log("TOKEN ERR", token);
                console.log("Error", err)
                alert("ชื่อผู้ใช้ หรือ หรัสผ่านผิด")
            });
        console.log("token", token);
    }

    return (

        <View style={{ margin: '10%', marginTop: "30%" }}>
            <Text style={{ marginBottom: 20, textAlign: 'center', fontSize: 24 }}>เข้าสู่ระบบ</Text>
            <TextInput style={{ marginBottom: 20 }}
                label='ชื่อผู้ใช้'
                value={email}
                onChangeText={value => setUserText(value)}
            />
            <TextInput style={{ marginBottom: 20 }}
                label='รหัสผ่าน'
                value={password}
                onChangeText={value => setPassText(value)}
            />
            <Button icon="" mode="contained" onPress={() => onSignin()} style={{ marginBottom: 20 }} color="#FFD700">
                <Icon name="user-alt"></Icon> เข้าสู่ระบบ
            </Button>
            <View style={{ justifyContent: "center", flexDirection: "row" }} >
                <View >
                    <Text style={{ fontSize: 16 }}>ยังไม่มีสมาชิกใช่หรือไม่ ?</Text>
                </View>

                <View style={{ marginHorizontal: 10 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Signup')}>
                        <Text style={{ fontWeight: "bold", fontSize: 16 }}>สมัครสมาชิก</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View >
    )
}
const styles = StyleSheet.create({
    scrollview: {
        flex: 1,
        backgroundColor: "#F7F7F7"
    },
    viewImage: {
        alignItems: "center",
        justifyContent: "center",
        marginTop: 25,
        marginBottom: 15
    },
    logoImage: {
        width: 180,
        height: 100
    },
    textInput: {
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 10,
    },
    signInButton: {
        margin: 30,
        borderRadius: 5
    },
    registerButton: {
        marginLeft: 30,
        marginRight: 30,
        borderRadius: 5
    },
})

export default LoginScreen