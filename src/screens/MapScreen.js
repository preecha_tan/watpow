import React, { useState, useEffect, useRef } from 'react'
import { View, StyleSheet, Dimensions, Text, SafeAreaView } from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service'
import { check, request, PERMISSIONS, RESULTS, openSettings } from 'react-native-permissions'
import { listTemple } from '../../api';

export default function MapScreen(props) {

    const [temples, setTemples] = useState([])
    // const [currentLocation, setCurrentLocation] = useState({})
    const [coordinate, setCoordinate] = useState({
        latitude: 0,
        longitude: 0,
    })

    useEffect(() => {

        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
            .then(result => {
                if (result === RESULTS.GRANTED) {
                    callLocation((position) => {
                        setCoordinate(position.coords)
                        MapViewRef.current.animateCamera({
                            center: {
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude,
                            }
                        })
                    })
                }
                if (result === RESULTS.DENIED) {
                    request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
                        .then(result => {
                            if (result === RESULTS.GRANTED) {
                                callLocation((position) => {
                                    setCoordinate(position.coords)
                                    MapViewRef.current.animateCamera({
                                        center: {
                                            latitude: position.coords.latitude,
                                            longitude: position.coords.longitude,
                                        }
                                    })
                                })
                            }
                            else if (result === RESULTS.DENIED) {
                                alert('Please accept')
                            }
                            else if (result === RESULTS.BLOCKED) {
                                alert('Thank you')
                            }
                        })
                    return
                }
                if (result === RESULTS.BLOCKED) {
                    alert('...')
                }
            })

        listTemple()
            .then(res => {
                setTemples(res.data)
            })
            .catch(err => {
                console.log("err : ", err);
            })
    }, [])


    const callLocation = (callback) => {
        Geolocation.getCurrentPosition(
            (position) => {
                callback(position)
            },
            (error) => {
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        )
    }

    const MapViewRef = useRef()

    return (
        <View style={{ flex: 1 }}>
            <SafeAreaView>
                <View style={{
                    height: '100%',
                    width: '100%'
                }}>
                    <MapView
                        ref={MapViewRef}
                        provider={PROVIDER_GOOGLE}
                        style={styles.map}
                        region={{
                            latitude: coordinate.latitude,
                            longitude: coordinate.longitude,
                            latitudeDelta: 0.015,
                            longitudeDelta: 0.0121,
                        }}
                    >
                        {
                            temples.map((item, index) => {
                                return <Marker

                                    coordinate={{ latitude: parseFloat(item.latitude), longitude: parseFloat(item.longitude) }}
                                >
                                    <Callout>
                                        <Text>{item.name}</Text>
                                    </Callout>
                                </Marker>
                            })
                        }

                        <Marker

                            coordinate={coordinate}
                        />
                    </MapView>
                    {/* <Carousel
                        data={allGarage}
                        containerCustomStyle={styles.carousel}
                        renderItem={(item) => renderCarouselItem(item)}
                        sliderWidth={Dimensions.get('window').width}
                        itemWidth={300}
                        removeClippedSubviews={false}
                        onSnapToItem={(index) => onCarouselItemChange(index)}
                    /> */}
                </View>
            </SafeAreaView>
        </View>
    )
}

const styles = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    carousel: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 48
    },
    cardContainer: {
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        height: 150,
        width: 300,
        padding: 24,
        borderRadius: 24
    },
    cardTitle: {
        color: 'white',
        fontSize: 16,
        alignSelf: 'center'
    }
});
