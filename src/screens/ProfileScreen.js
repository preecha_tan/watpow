import React from 'react'
import { View, Text, SafeAreaView } from 'react-native'
import { IconButton, Button } from 'react-native-paper';

import NotLogin from '../components/notLogin'
import { useSelector } from 'react-redux';
import IsLogin from '../components/isLogin';

export default function ProfileScreen() {

    const token = useSelector(state => state.token)

    return (
        <SafeAreaView>
            {
                token === "" || token === null ?
                    <NotLogin />
                    :
                    <IsLogin />
            }

        </SafeAreaView>
    )
}
