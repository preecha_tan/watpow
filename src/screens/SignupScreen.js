import React, { useState } from 'react'
import { View, Text } from 'react-native'
import { IconButton, Button, TextInput } from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { signUp } from '../../api';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default function SignupScreen(props) {

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [firstname, setFirstname] = useState("")
    const [lastname, setLastname] = useState("")

    const onSubmit = () => {
        signUp(firstname, lastname, email, password)
            .then(res => {
                props.navigation.navigate('Login')
            })
            .catch(err => {
                alert("กรุณากรอกข้อมูลให้ครบ", err)
            })
    }

    return (

        <View style={{ margin: '10%', marginTop: "20%" }}>
            <Text style={{ marginBottom: 20, textAlign: 'center', fontSize: 24 }}>สมัครสมาชิก</Text>
            <TextInput style={{ marginBottom: 20 }}
                // mode='outlined'
                label='ชื่อผู้ใช้'
                value={email}
                onChangeText={value => setEmail(value)}
            />
            <TextInput style={{ marginBottom: 20 }}
                // mode='outlined'
                label='รหัสผ่าน'
                secureTextEntry={true}
                value={password}
                onChangeText={value => setPassword(value)}
            />
            <TextInput style={{ marginBottom: 20 }}
                // mode='outlined'
                label='ชื่อจริง'
                value={firstname}
                onChangeText={value => setFirstname(value)}
            />
            <TextInput style={{ marginBottom: 20 }}
                // mode='outlined'
                label='นามสกุล'
                value={lastname}
                onChangeText={value => setLastname(value)}
            />
            <Button icon="" mode="contained" onPress={() => onSubmit()} style={{ marginBottom: 20 }} color="#FFD700">
                <Icon name="user-alt"></Icon>สร้าง
        </Button>
            <View style={{ justifyContent: "center", flexDirection: "row" }} >
                <View >
                    <Text style={{ fontSize: 16 }}>มีสมาชิกแล้ว ?</Text>
                </View>

                <View style={{ marginHorizontal: 10 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Login')}>
                        <Text style={{ fontWeight: "bold", fontSize: 16 }}>เข้าสู่ระบบ</Text>
                    </TouchableOpacity>
                </View>
            </View>

        </View >

    )
}
