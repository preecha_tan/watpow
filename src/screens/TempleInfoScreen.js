import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, SafeAreaView, Image, ScrollView, RefreshControl } from 'react-native'
import { Header, Left, Body, Right, } from 'native-base';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useNavigation } from '@react-navigation/native';
import { Badge, Button, Modal, Portal } from 'react-native-paper';
import { TextInput } from 'react-native-paper';
import { useSelector } from 'react-redux';
import { createMilestone, listMilestone } from '../../api';
import { setToken } from '../reducers/LoginAction';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';


export function TempleInfoScreen(props) {

    const item = props.route.params.item
    const temple_id = item.ID

    const navigation = useNavigation()

    const [showModal, setShowModal] = useState(false)
    const [eventname, setEventname] = useState("")
    const [description, setDescription] = useState("")
    const [dob, setDob] = useState(new Date())

    const [milestone, setMilestone] = useState("")

    const parseDate = moment.utc(dob, "yyyy-MM-dd")

    const token = useSelector(state => state.token)

    const navigate = useNavigation();

    const [refresh, setRefresh] = useState(false)

    //date time picker
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const handleConfirm = (date) => {
        console.warn("A date has been picked: ", date);
        setDob(date)
        setDatePickerVisibility(false)
    };
    //date time picker
    const fetch = () => {
        setRefresh(true)
        listMilestone(temple_id)
            .then((data) => {
                setRefresh(false)
                setMilestone(data.data);
                console.log("dsadasdasdasdsd", data.data);
            })
            .catch((err) => {
                console.log("ERROR", err);
            });
    }

    useEffect(() => {
        fetch()
    }, []);

    const _renderMilestone = ({ item }) => {

        return (
            <View style={{ marginBottom: 15 }}>
                <Text style={{ fontSize: 16 }}>วัน : {item.eventname}</Text>
                <Text style={{ fontSize: 16 }}>รายละเอียด : {item.description}</Text>
                <Text style={{ fontSize: 16 }}>วันที่ : {moment(item.date).format("MMM Do YY")}</Text>
            </View>
        )
    }

    const onSubmitMilestone = () => {
        console.log("createMilestone");
        const data = {
            "eventname": eventname,
            "description": description,
            "date": parseDate
        }
        console.log("daat", data);

        createMilestone(temple_id, data, token)
            .then(response => {

                console.log("ressssssssssssssssssssssponse", response.data);

                // dispatch(setToken(response.data.token))
                // localStorage.setItem("token", response.data.token)
                if (token != "" || token != null) {
                    setEventname("")
                    setDescription("")
                    setShowModal(false)
                }
            })
            .catch(err => {
                console.log("Err have token : ", token);
                console.log("Error", err)
            });
        console.log("id", temple_id);
        console.log("desc", description);
    }

    return (
        <SafeAreaView style={{ flex: 1, height: '100%' }}>
            <View>
                <Header style={styles.header}>
                    <Left>
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                        >
                            <Icon name="chevron-left" size={28} color="black"></Icon>
                        </TouchableOpacity>
                    </Left>
                    <Body style={styles.body}>
                        <Text styles={styles.text}></Text>
                    </Body>
                    <Right>
                        <Button style={{ marginRight: 15 }} icon="plus" mode="contained" color='#FFD700'
                            onPress={
                                token === "" || token === null ?
                                    () => navigation.navigate('Login')
                                    :
                                    () => navigation.navigate('CreateEvent', { item })
                            }
                        >
                            <Text>สร้างกิจกรรม</Text>
                        </Button>
                        <Button style={{}} icon="plus" mode="contained" color='#FFD700'
                            onPress={
                                token === "" || token === null ?
                                    () => navigation.navigate('Login')
                                    :
                                    () => setShowModal(true)}
                        >
                            <Text >เพิ่มวันสำคัญ</Text>
                        </Button>
                    </Right>
                </Header>
            </View>

            <View style={{ height: '100%' }}>
                <Image
                    source={{ uri: item.image }}
                    style={{ height: '25%' }}
                ></Image>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }} refreshControl={<RefreshControl refreshing={refresh} onRefresh={fetch} />}>
                    <View style={{ marginLeft: 20, marginBottom: 10 }}>
                        <View style={{ flexDirection: 'row', marginBottom: 15, marginTop: 20 }}>
                            <Text style={{ fontSize: 24, }}>{item.name}</Text>
                            <Badge danger style={{ marginLeft: 10, marginBottom: 5 }} >
                                <Text style={{ fontSize: 14 }}>{item.type}</Text>
                            </Badge>
                        </View>
                        {item.opcl === "" || item.opcl === null ? <Text style={styles.infocontent}>เวลาเข้าชม : - </Text> : <Text style={styles.infocontent}>เวลาเข้าชม :  {item.opcl}</Text>}
                        {item.location === "" || item.opcl === null ? <Text style={styles.infocontent}>สถานที่ : - </Text> : <Text style={styles.infocontent}>สถานที่ : {item.location}</Text>}
                        {item.district === "" || item.opcl === null ? <Text style={styles.infocontent}>จังหวัด : - </Text> : <Text style={styles.infocontent}>จังหวัด : {item.district}</Text>}
                        {item.tel === "" || item.opcl === null ? <Text style={styles.infocontent}>เบอร์โทรศัพท์ : - </Text> : <Text style={styles.infocontent}>เบอร์โทรศัพท์ : {item.tel}</Text>}
                        {item.website === "" || item.opcl === null ? <Text style={styles.infocontent}>เว็ปไซต์ : - </Text> : <Text style={styles.infocontent}>เว็ปไซต์ : {item.website}</Text>}
                    </View>

                    {/* <Button style={{}} icon="camera" mode="contained" onPress={() => setShowModal(true)}>
                        <Text style={{ fontSize: 18 }}>เพิ่มวันสำคัญ</Text>
                    </Button> */}
                    <View style={{ marginLeft: 20 }}>
                        <Text style={{ fontSize: 24, marginTop: 10 }}>กิจกรรมวันสำคัญ</Text>
                        <FlatList
                            data={milestone}
                            renderItem={_renderMilestone}
                            keyExtractor={item => item.id}
                        />
                    </View>
                </ScrollView>

            </View >
            <Modal visible={showModal} onDismiss={() => setShowModal(false)}>

                <View style={{ marginLeft: 15, marginRight: 15, justifyContent: 'center', height: 350, backgroundColor: 'white', borderRadius: 10 }}>
                    <Text style={{ fontSize: 22, textAlign: 'center', margin: 10 }}>เพิ่มวันสำคัญ</Text>
                    <TextInput style={{ margin: 10 }}
                        label='เทศกาล'
                        value={eventname}
                        onChangeText={value => setEventname(value)}
                    />
                    <TextInput style={{ margin: 10 }}
                        label='รายละเอียด'
                        value={description}
                        onChangeText={value => setDescription(value)}
                    />

                    <View style={{ margin: 15 }}>
                        <Button title="Show Date Picker" onPress={showDatePicker} >
                            <Text>เลือกวันที่จัดกิจกรรม</Text>

                        </Button>
                        {/* <Text>{dob}</Text> */}
                    </View>
                    <Button style={{ margin: 15 }} mode="contained" onPress={onSubmitMilestone}>
                        <Text>เพิ่ม</Text>
                    </Button>
                </View>

                <DateTimePickerModal
                    isVisible={isDatePickerVisible}
                    mode="date"
                    onConfirm={handleConfirm}
                // onCancel={hideDatePicker}
                />
            </Modal>

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        minHeight: 360,
    },
    headertext: {
        fontSize: 24,
    },
    body: {
        alignItems: "center",
        justifyContent: "center",
    },
    header: {
        borderBottomWidth: 0,
        borderBottomColor: 'black',
        // backgroundColor: '#FFD700',
        backgroundColor: 'transparent',
    },
    infocontent: {
        fontSize: 16,
        marginBottom: 10
    }
});

const testimg = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTEhMVFRUWGBUXGBUXFRUXFxcYFxcXFxUVFRcYHSggGBolHRcVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0lICUtLS8tLS4tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAKgBLAMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAADBAECBQAGB//EADUQAAEDAwIEBAUEAgMAAwAAAAEAAhEDBCESMQVBUWEicYGRBhOhscEy0eHwQvEUUmIHIzP/xAAaAQACAwEBAAAAAAAAAAAAAAACAwABBAUG/8QAKREAAgICAQQCAQMFAAAAAAAAAAECEQMhEgQTMUEiUfBxgbEyYaHh8f/aAAwDAQACEQMRAD8A+HKQ6BjfM+WMfdQoUISuChSrRC7GyYCsxUARGH17FHEpjLHeFXYgtKPTCfEUy6vQYqkJ6gyGT3/CbFWxU5UhG9PiSwR62esyZziMRA9/oubSQNNsOOkKFqkU0y4wcb+QP0KoGEoXDYdgdldlAlGZTAV3PhWsf2U5fQjXbGELSm/+OXZUst0HBthcqFHBVhaJs5QKtqQqlikiKaFUY25iQoNB3RTSrlvkhSS/qLb+irR1CI1n+kcAHKat7IO3PXPeMBE4NFxdijGlMtZOmekfU/urOty10HH5RWbtA/p/oSZKjZhV+ROrSgkDO4n8qlJmU09xknmZVaDZKOtFUuWhy1o8kC8ttJE9vqtKxIDhjmPUd0nxUn5jhkwfTsY8krd0bJwj27M+q1BITBMIbWz5c1VmZoGAhvajNVXhUC0ALVQhHLUN6sBoCVUojgoDJVimihChSuUBJd7rlCsCrRDgEQKgV2hMiUwrE5SbhK0wnqLVoxoTNlnUyIkROR3HVNaopkdShaNlaoMQnpUZ5boWbTlXczCPSYuqM5K1EvlsTDFLj0RHtQ4VDLsogP3/ACmiEP5aXOIaZ1FxGyao5KA1qbtG7lFAXN6DAiErXeByymi3CWq0pTZt0LjVmXWJduSUEMWjUpQhCmscobNKkDoNIT9tUIQKbUdjRIn0P7o4qkU3bNEw8DqgWrQ14DhzG/LIn1wQhhxBhatBgqUXFwBdTLXB0CdOQQf+2S3fos2bR1ej+f6r/JlXbIeR0Kig2F1V5cSTzKJbUi7bkipKOwVbyviPW24gTyReOWoYZIjUA70IBWlwLhhe5uMCScchJJKyfiGuXE+Z/hZ57nSOrx49O3LyYl2BgDnn+Eq45yiMJBnoqlpLoOOqjVHLbvZzmneN0Mu91as//XQILiqQLZD3qpXEqJVi2yjlUlc4qFYtsgqFK5QA5WChSESIWCI0IYRWJkQWMUmrQoNSVEJ6mcLXjM+QZoskotejDoUWu48wneJ0dL1rUbiYnOp0L/LGkHnt5D+lBfTTcSFRzcK3EuMjOLMobqSeNNQ5mUlwHqYkymqkLSpUkB9PJUcHRamhZtNNUqeE5aWRNMny+6m2tT7BFHGxcsqJpW8sHmlX0N/NbVtTmB2WdcDJCbOCoVGdsx7iAl3Honrqhz80CrTWScWa4yFaTym2nCWYxO024SfCoco29EE7LW4JnW04DmmT5ZH2CzxSGgnmCMdUxZV9LgOuD6rPl+UWkdbo49uacvz0LVaZye62vh6hBBIkEwfLE/dRQ4cXPgAmTstC1vWUZhgc6ME8j1A2PqkTyco8UdLp+m7c+5I9zwfg5tqFes6B4HBh3BkQCOuCvj/F/wBR6fleorfEFV7dD3Oc2DguJjENPoYWBxl7GmJ1EATyGqMjvG3oqhaaL6lKUG2/z6MdoG5wPz0QatfMgRH17mVWrVSjno3E5EppaRJfJXFR+yvSplyoWrYMqrgiwhuVgsEQoUlQrFHBSqqQqKJXBQpRECMcRtzx7q9NCBRGJkWUxykU3Sckaa0aIgbb/TyWrGZ5jIfkLQu3yVmkGQnGGXLZB+jFNbTGtGx6/vzVXMRo8XYH08k9VogkHqBA9E7iZ3kox9Ks6ktYWUFVfaKcNE76MwMhVpW+pyeew7HkptWw6fT7qcCd3TNCwpgUo7flCuKQAdCYtGyw/wB5pes+SG90YpO2Tb0oDD2WVeU/HK9M+3gN7LOrWUgnzQSjaDx5EmY1WnLQOkpS7ochnb7LSvbfS4EK9zTGmeqy5DoYqZ5s0UxTEQmKtvjHNO8N4Y6o9rGtJc4gADnKw5JJLZ1umxOU6QnTbPJM8P4bJ1OxG62OKW1Jr20qWQwQ58zrf/kR/wCQcDsJ5oVyCzw7CAfcSPusPOTdL2d7H08IrlP0adarSbS0sk1HnLjgBo5DM5O5PReYuquTnqOyY+bIndIXu4C1w6fgrMWfru46QamR+ucN+sbfVYV1VLnEnnK06/hpxHr+6xnndBBe2L6mfiK/cBVCVcVd70JyqRgcjgUVrygBXBQ0UpBy9CeVCgq6LcrKKFYqFACq5SuQlHKVVSrISitOUIK7EUSMcobrXt2yFj0Vs2eBK3YDJm8DtvT1McIkjI7ZymbO2nb+/wApaxqw+Qtm90seNO2D6kfyuhBKrOdklJPivYA0TvzT1OjJznpCNZN17AAjfvhXYPEmmSUn4GNMtQqjJTQYYg7FK3AIRGYRdTyopsx9fv8AuUZzDmeykNk+iFjVJhbNkNHdDp2rjU8jKcoUiQAFoULfOOypkWR2XqUJA8ilBa5hb9O2JA7hc6105jeUvmg4JpnkuK2XPosmtRlsL295Y6hhKW/Bp8JwZGTt6pWSSq2dTpYyclFHkqdoTpABnryjpH57r04tv+PSkD/7qjfVlNw37OeJ8mn/ANY17TgLaWp9UAhmw3D3HLWyN28z28wqVqLqkvd+okydp/oXGy1OX9j2fS4+zj/v+fj/AOmBwHhLqlZgcIBP5CF8R2xdVcOnhjs0BoH0XqrKq23JdEvnE8v52WO9mqoScyZz9VeONzsDNkccTi/Z4x1MyAFW/DGxzMey9LecOAMczt035ry1zZPNSOp39Vsl4ObDTpLfoBVaDSd/2mR3EeL2x7lYVWcjlzW3c0TrJGwMN9EhxOlpMCIcAfz/AAhULjbE58nzpfn2YbwqI1UQgwsslsWcpAUgK8KKJLKqFdwUQrohSFEK8LoVUQCuUwoSyHKSoXKELK7FRFotlHHyRjNuMrXMBoAWbas8QWjphbsK0ZcvkPbHIWs9+poJ32WNSctlvibstmN6MWXTTNXhVcNMHYiP9J5zIz0x5rEt3xC9FaP8ORIIP25LQmYM0Kdomi7HeVNalKJZUZBzsrtbyRIw5JVLRo8OsGVqJp6QKoJcx3N+PEw9TzHqOawqNgSTyhaYqlsFpgtMgj0WvdWnzWiuwQTio3YBx/yA6O385Sm+L34YyEnKOvK/j/X8foZ1rbta0dwnLagMq1OiBjkBufueiyrDjT3XjaDAx1NzoBEzETMgxjySpZKQ/BilNs9RRp4CDXqNa4a3NbJganAT2E7rYqUIgQsT4o+G3Vgx1MiQCCCYEHIO3KT9OiyrIjoxw7pivHeOULYHV4qnKmN/Nx/xH17LEsOM/NcDVuqVKf8ABjHlwnk5726WnvJXn6/Cbiq6q1tMudRn5hkAMDZEuc4gf4n2Xng4g75Scyk1SZ2+hnjxytr99H2uhaQyCfmas6ifYtd0Q/kbCP7EL5pwL4rq27o/UznTJwR2/wCp7+84Xu6XxtZEAl7gT/iWOJB6S0ELLHWmdeeZy3F3+fRHFqUuiII5emPwg2tsC0kiCIjM8jK06F/Quc03TH/kj1znoiXVAggREH2wnJqqATuWzzxtyXe6x7+3AmBknfy3Xqr+lpaOuYPPcrC4i0CMY6Jt2gIRSto8reOAY4R0M/decuHzg+i9HxQ+GAvNXTCD3CapaOX1EflZl3DkCEasxCWSXkFEhXhdTCKGdFaRATmKpCMVRwUaICUwrQphDRYouXLkgs5SoUqEDW9Au2TrGhrQOaHY1tOOoMrm/laoJJX7FytsbtiJ8ky6oI+yy5IlGZVkJ0Z+hcoXs0GtwDO+YzjK1bGuQFi0zstO2EtlacbMuWOtmxSaHAkHZbdhgNlYVm3wStrhrwBDh5LUjn5vDQ9ZSCD3WmylJEDtjmps7UOgjG260yG0m+Ey88xyH7pc8laXkwvHzfJ6Qm6nTaYcCTPLCetLzTgCGkRp6g9fofRZDsuWlQp7E/0IJwVbLwzfL46EeJ0tdN+tlTAdpAaflgwYcSDkDqVl/A/DnOug9roFOSY3M4DY7/hbHxPeubQDY0ioYHiGp4Akw0ZDZLd4PZZ/B/hvidI/NogUzAMF7JcJPhLTIkbwY3S5SXHdI6vTwaej6iaMwV3yVl/CnGalx82nXp6KtEtD42JOobciC09l8wv/AIwu2XtcsrP0is/TTJluhjnAaQcRDRIG8rCscrcfo6SlGlJHt/gy0FSpxGo5oLKly+lByHNp6muBB3B1FeM/+Q/gkWwFe3J+U5waWEyaZMkEOO7MRnIMZM4f4f8AGnyLQU2VKYqE1X+JrpDnvc86tPKXYOcbry/E/jW5rUfk1XNqtDtUuaJJExJbEjxfQI3GSdhQyKqPPXBY0AAancyXgt57BkRy3JQNRxnHQYHrCrUE5kZ5AAfQKWsxPTy+g5qKNju7R9D+CuOWlCgdb3Co79Q0OOBtEYj6rR4n8d248NNr6p6xobju7P0XzKg7psmbaxq1HRTYSfL79PVX2/Y3v6Pa2vxUa9QCp8umwNOJwTsBqPPM8tkrxniEkgFpAkbg/ZK2/wAKva0Gr5kdPXb7pXi1MNy0Y2/2jjjCXV0jLvK89llVXt1AmY5ol3WJKTrORRjRly5XLQpdtGo9EiU5WHNLFhSMnkqJDcIwMwFSZ9MIjTA7qohMlzIwUIq7noRVSaIcoXLkBBRcuXJARytKhSrRCWmEWm5CV2o4spl3ORqG4S7SmqDU2G2Cx6jkrSp1sAAbfXzSFAxlMUjlbo6Mk1Z6XhERB5r0NjZ4J5LzvCWzC9rTAZQM7mI+qe5VRyM2pMvRfiNgorVDIg891m06xTtMattwmpJGOcXdsas2yc5P7re4dZlzgSJ7LEbc02fqcNQ5CCe08m+sKnF/jF1FsUiKZIy5wDnDs1vXzWfKpS/pNXTRincjRoNpPvala4exlGzDWDWQGmq4aic82z7hq0H/AB1b6g2iypWZPiqNEMEb6Sf1R6L4xxnjJquLnFzySTqeZydy0bNM9kpT4rUIILjgR+35SJYot/I60XNQ+Oj3nHPik/Mrmk75bLh1Jxj/APQfLAiCMAEyee+RyXheKcRdVdJa0GSZa2JJ3J6eQwln1SMk7pcGVbpaRIRflsYLyWwSlQ5S8QmRaF51MHhgEyQA08wUDVjLoG2mXDDdt4n3Kq3J7pihW0OIkGcK9O3OrUBid+hPL7qlEcpntPhfgVu+i2q4F5IJ07NBkg4G+y9LRqMZAADQOQEBed+GqtRjCyoCBOpsxz378h7priV5AxviT59E143IGGZQk2w3H7yduYXi7ysQ49xB/ZbF9WJa107YWDf1J7IuHEjzxyoyq9HP5SF4YwtB1cAEb7LJv3y7CCbpAwtsVc+VIb0VxRVdlnr7HkaIB69FGpSUMoXoIgqCpKhAQhcpUKixVcpXJBZykKFYK0QsAphc1XAlNSKKgJ+kcJNjcp1jcJ2JASHLTeEwxuUlb1dJTltW8Q81ri0Z5J2eo+HKcuXpuM3bWgAuAAA5r5+/jDqZLGQM5O5MJWveueZcc9dz/CZyV39GCXTSnK34PXP4u0CWguPIZE+WJKva37nu8TtI/wCrd+8wYHqSey8jRuHbNxO55n1Vv+VpB0kzAk/eOyLuX5I+nXheT0vE+LtB004ZkxEkjzJ/T6QsB9052SZk+yzWVCcczlGaYGekoedjYYFAHdvzI7ItODT1CAQYOckHb2j6oDiT+nJIg45YM9tkwy0jwnJ6Nz7lA1bHOkgbsogpRvjp1RNUCG49JPuqUxO6nEqxepXg7CQfREFUuDo3wcexVatCTAUUfCRPX6HCCnYVqgzLVzoMEr1XCqALabX7NOojynB91k2FydIAH97pt95p2J807HjXkzZMsvBu3V4N2iAEjUvRpM5lZjbycSg3lbun0hSs1m1PmM3w1ed4rU8RhFpXxAiee3JZt7W1SUrJQzBBqQJzc56fylXsySUakZBVKxEws0jdFuxaoUNxRaxHJAJSZD4lXOVVYqqUwiSoXLlRCFBVlBVEFlMKVISQysKQphSArRRICuxVV2HKYimXhNtcISerKI10J0JUA1YckDnJ59OUeu6ZtTmf7KzmuTIqQEyEgJRCVSSSTuUeg3GUtRyUxqTY/YqX0WbU5DfqhfMjfkqAxHmrOOVCJF7dsnoO6O90jeY5/wAKlAEnKIKQA329vRMSdASasLSpF2223QJurSIDfLYflH4ZVGB2Q69QNLwc7x0H7piiZXkblQmwwuLkJz5UvdHspQ0OykcLr2jpAkgk58kO2uo9kJ1WTJVNKiqlZpm6DWhoxge6SqXEpSrVVDUUc/RI4vY/RqZwppO1ODRzPP8AlKUqkZVrar4ge6JT8FOHkvdkNcR0wkqrla9qy4+ZUU4iT7JMpWx8I8YpsmgQGk+X97pe4eiVDgpZyVN+h0Vuwbih6lLnclULK2PRy4qVBKohClVXKiyZULlyogJcpXJQRykBQuRFF2hSAuXI0UF0qxKhcmeAShKMDjyXLlIsjJpvhHpPlcuTsbd0LktFnBQ1wUrk1vYKVoO2rAUg9Vy5MsS0O2L8gDc81N9cw4wB5/75rlyO9COK7gj/AMhxBBJI3iT7qS6QuXIEzRxSKswocVK5R+ClvYvWeuplSuSU9ja0XbU5IjHc8LlyOMinFC1V8mfJVLlC5LbGJBWuwqupSCQuXK2V4ECF0KVyy0OJKhcuUZCi5cuQFnKJXLkJZ//Z"


export default TempleInfoScreen