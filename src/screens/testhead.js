import React from 'react'
import { View, Text, StyleSheet, SafeAreaView, ScrollView, FlatList, Image } from 'react-native'

import { Avatar, Button, Card, Title, Paragraph, Searchbar } from 'react-native-paper';


import { useNavigation } from '@react-navigation/native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Header, Left, Body, Right } from 'native-base';

export function HomeScreen() {

    const navigation = useNavigation()

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View>
                <Header style={styles.header}>
                    <Left>
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                        >
                            <Icon name="chevron-left" size={28} color="black"></Icon>
                        </TouchableOpacity>
                    </Left>
                    <Body style={styles.body}>
                        <Text styles={styles.text}></Text>
                    </Body>
                    <Right></Right>
                </Header>
            </View>
        </SafeAreaView>

    )
}
const styles = StyleSheet.create({
    headertext: {
        fontSize: 24,
    },
    body: {
        alignItems: "center",
        justifyContent: "center",
    },
    header: {
        borderBottomWidth: 0,
        borderBottomColor: 'black',
        backgroundColor: '#FFD700',
    }
});

export default HomeScreen